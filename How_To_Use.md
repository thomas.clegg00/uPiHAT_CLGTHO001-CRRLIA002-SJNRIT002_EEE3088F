How To Use this controller PiHat:

This PiHat is designed to be a lights controller board with an input sensor and trigger switch.

	J7 is the supply connector, the supply should be between 12V and 20V
	J6 is the connector for the IR sensor HC-SR501
	J1, J2, J3 are the supply connector used to supply 12V to external lights 1,2 and 3
	J4 is the NO Relay connector used to switch an external circuit, this is optional

	D1, D2, D3 are the LED indicators for J1, J2, J3 respectivly
	D4 is the LED indicator for the external trigger J4.


The PiHat GPIO connections:
Input Pins
	GPIO03: IR sensor input

Output Pins
	GPIO06:	External Trigger
	GPIO13:	Lights1
	GPIO19:	Lights2
	GPIO26:	Lights3

The Pi unit will control these pins to control the external lights connected, while recieve an input from GPIO003 from the IR sensor.
The total current rating of the lights should not exeed 10A