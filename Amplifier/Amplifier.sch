EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:OPAMP U1
U 1 1 60B94384
P 4750 3800
F 0 "U1" H 4750 3550 50  0000 L CNN
F 1 "LM358" H 4750 3650 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket_LongPads" H 4750 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 4750 3800 50  0001 C CNN
	1    4750 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 60B99513
P 4300 4250
F 0 "R2" H 4232 4204 50  0000 R CNN
F 1 "1k" H 4232 4295 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 4340 4240 50  0001 C CNN
F 3 "~" H 4300 4250 50  0001 C CNN
	1    4300 4250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R1
U 1 1 60B977DC
P 4300 3300
F 0 "R1" H 4368 3346 50  0000 L CNN
F 1 "1k" H 4368 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 4340 3290 50  0001 C CNN
F 3 "~" H 4300 3300 50  0001 C CNN
	1    4300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3700 4300 3700
Wire Wire Line
	4300 3700 4300 3450
Wire Wire Line
	4300 3700 4300 4100
Connection ~ 4300 3700
$Comp
L power:GND #PWR01
U 1 1 60BA0375
P 4650 4500
F 0 "#PWR01" H 4650 4250 50  0001 C CNN
F 1 "GND" H 4655 4327 50  0000 C CNN
F 2 "" H 4650 4500 50  0001 C CNN
F 3 "" H 4650 4500 50  0001 C CNN
	1    4650 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 4400 4650 4400
Wire Wire Line
	4650 4400 4650 4100
Wire Wire Line
	4650 4400 4650 4500
Connection ~ 4650 4400
Wire Wire Line
	4650 3500 4650 3050
Wire Wire Line
	4650 3050 4300 3050
Wire Wire Line
	4300 3050 4300 3150
Wire Wire Line
	4300 3050 4000 3050
Connection ~ 4300 3050
Wire Wire Line
	4450 3900 4000 3900
Text GLabel 5550 3800 2    50   Input ~ 0
GPIO_Amplifier
Text GLabel 4000 3050 0    50   Input ~ 0
3V3_Regulator
Text GLabel 4000 3900 0    50   Input ~ 0
IR_sensor_Output
Wire Wire Line
	5050 3800 5550 3800
$EndSCHEMATC
